<?php

$name = $_GET["name"];
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="HTML Tidy for Linux (vers 25 March 2009), see www.w3.org" />

  <title>Window Maker: Dockapps</title>
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta http-equiv="refresh" content="5; url=http://www.dockapps.net/">
  <link href="../title.css" rel="stylesheet" type="text/css" />
</head>
<?php
echo "<body>\n";
include("../dock.php");
include("../header.php");?>
 <div>
    <table class="inner" border="0" cellpadding="1" cellspacing="1">
      <tr>
        <td content="content" colspan="2" valign="top">
          <br />
          <br />
<h1><a href=".">Dockapps</a></h1>

The dockapps page has been moved to
<a href="http://www.dockapps.net">dockapps.net</a>.

You will be redirected shortly.


<?php
include("../footer.php");
?>
